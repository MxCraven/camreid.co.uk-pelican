Title: About Me
Date: 2019-03-26
Sortorder: 001

Hello there! My name is Cam Reid, and I'm a cyclist, gamer, and programmer, 
and a whole bunch of other things on top. I'm an age of numberical value > 
18, and I'm an enby :3 (gender neutral pronouns, but not fussed really). 

This website is written using [Pelican](https://getpelican.com), and after 
a lot of messing about to get it exactly how I like it, I really like it :D 
Much quicker than WordPress, and for me as someone who is fine with just 
SSHing into my server to make a blog post, it's great. 

There are links to all my socials and contact info down in the bottom section 
and I hope to see you around!

-Cam
