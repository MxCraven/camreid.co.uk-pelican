Title: All Setup... I think?
Date: 2019-03-27

So, after a lot of work on themes and that, I think I've got it how I like it. 
Using a new font I've never used before, Fira Mono, because it looks really 
really nice, but I wouldn't want all the fancyness in my terminal. However, 
DejaVu Sans, which is my default monospace and everything font, doesn't look 
all nice and "web design" as it were, so I changed it to Fira which looks very 
nice :D

So, that's it I guess. I've forked a theme and hacked it to shit. It's 
basically unusable for anyone but this project due to the fonts but I guess 
that's fine for me. Maybe if there's some interest in my changes I'll make a 
PR or I'll change my own repo to have the changes and use default web fonts. 
All things that can be done :3 

Anyway, I should be getting on. Enjoy my website! This should be visible 
for the world now :D
