#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Cam Reid'
SITENAME = 'Cam Reid'
SITEURL = ''
THEME = 'blue-penguin'
#THEME = 'notmyidea'

PATH = 'content'

TIMEZONE = 'Europe/London'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
# ~ FEED_ALL_ATOM = None
# ~ CATEGORY_FEED_ATOM = None
# ~ TRANSLATION_FEED_ATOM = None
# ~ AUTHOR_FEED_ATOM = None
# ~ AUTHOR_FEED_RSS = None

#blue_penguin theme settings

CONTACT = (
    ('Matrix (@crabcrabcam:matrix.org)', 'https://riot.im/app/#/user/@crabcrabcam:matrix.org'),
    ('IRC (ccCam on most servers)', 'https://kiwiirc.com/nextclient/'),
    ('Email', 'mailto:cam@camreid.co.uk')
)

# Social widget
SOCIAL = (
    ('Mastodon (NSFW)', 'https://radical.town/web/accounts/74628'),
    ('Twitch', 'https://twitch.tv/ccCamTV'),
)


DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

PLUGIN_PATHS = ['plugins']
PLUGINS = ['pin_to_top']
STATIC_PATHS = ['fonts', 'images']
EXTRA_PATH_METADATA = {
    'images/favicon.ico': {'path': 'favicon.ico'}
}


DISPLAY_CATEGORIES_ON_MENU = False
PAGE_ORDER_BY = 'sortorder'
